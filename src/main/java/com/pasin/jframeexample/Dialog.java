/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Pla
 */
public class Dialog {
    private static JDialog d;

    public Dialog() {
        JFrame f = new JFrame();
        d =new JDialog(f,"Dialog",true);
        d.setLayout(new  FlowLayout());
        JButton b = new JButton("OK");
        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Dialog.d.setVisible(false);            }
        });
        d.add(new JLabel("Clicked Button to vontinue"));
        d.add(b);
        d.setSize(300,300);
        d.setVisible(true);

    }
    public static void main(String[] args) {
        new Dialog();
    }
    
}
