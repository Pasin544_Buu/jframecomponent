/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Pla
 */
public class MenuItem1 extends JFrame{
    JMenu menu, submenu;
    JMenuItem i1,i2,i3,i4;
    MenuItem1(){
        JFrame f =new JFrame("Menu and MenuItem");
        JMenuBar mb = new JMenuBar();
        
        menu = new JMenu("Menu");
        submenu = new JMenu("submenu");
        i1 = new JMenuItem("Item 1"); menu.add(i1);
        i2 = new JMenuItem("Item 2"); menu.add(i2);
        i3 = new JMenuItem("Item 3"); menu.add(i3);
        i4 = new JMenuItem("Item 4"); submenu.add(i4);
        
        menu.add(submenu);
        mb.add(menu);
        f.setJMenuBar(mb);
        f.setSize(400,400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new MenuItem1();
    }
}
