/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

/**
 *
 * @author Pla
 */
public class ScrollBar1 {
    ScrollBar1(){
        JFrame f =new JFrame("ScrollBar Ex");
        JScrollBar s = new JScrollBar();
        s.setBounds(100,100,50,100);
        f.add(s);
        f.setSize(400,400);
        f.setLayout(null);
        f.setVisible(true);
    }
    public static void main(String[] args) {
        new ScrollBar1();
    }
}
