/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Pla
 */
public class Separator1 {
    JMenu menu,submenu;
    JMenuItem i1,i2;

    public Separator1() {
    JFrame f =new JFrame("Separator");
    JMenuBar mb =new JMenuBar();
    menu = new JMenu("Menu");
    i1 = new JMenuItem("item 1");
    i2= new JMenuItem("item 2");
    menu.add(i1);
    menu.add(i2);
    menu.addSeparator();
    menu.add(menu);
    mb.add(menu);
    f.setJMenuBar(mb);
    f.setSize(400,400);
    f.setLayout(null);
    f.setVisible(true);
    }
    public static void main(String args[]){
        new Separator1();
    }
    
}
