/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/**
 *
 * @author Pla
 */
public class Combobox2 {

    JFrame f;

    Combobox2() {
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        f = new JFrame("ComboBox");
        f.setSize(400, 400);
        String Team[] = {"EG", "OG", "Alince", "T1", "Fanatic"};
        final JComboBox cb = new JComboBox(Team);
        cb.setBounds(100, 100, 90, 20);
        f.add(cb);
        f.add(label);
        
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton b = new JButton("Show");
        b.setBounds(200, 100, 75, 20);
        f.add(b);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Team Dota2 Selected = >>> "+cb.getItemAt(cb.getSelectedIndex());
                label.setText(data);
            }

        });
    }

    public static void main(String[] args) {
        new Combobox2();
    }
}
