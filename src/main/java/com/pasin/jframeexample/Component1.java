/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author Pla
 */
class Component2 extends JComponent{
    
    public void paint(Graphics g){
        g.setColor(Color.GREEN);
        g.fillRect(30, 30, 100, 100);
    }
}
    
    public class Component1{
        public static void main(String[] args) {
            Component2 com =new Component2();
            JFrame.setDefaultLookAndFeelDecorated(true);
            JFrame frame = new JFrame("JComponnent");
            frame.setSize(300,200);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.add(com);
            frame.setVisible(true);
        }
    }

