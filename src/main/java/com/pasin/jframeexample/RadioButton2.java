/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author Pla
 */
public class RadioButton2 extends JFrame implements ActionListener {

    JRadioButton r1, r2;
    JButton b;

    RadioButton2() {
        r1 = new JRadioButton("Male");
        r1.setBounds(100, 50, 100, 30);
        r2 = new JRadioButton("Female");
        r2.setBounds(100, 100, 100, 30);
        ButtonGroup bg = new ButtonGroup();
        bg.add(r1);
        bg.add(r2);
        b = new JButton("click");
        b.setBounds(100, 150, 80, 30);
        b.addActionListener(this);
        add(r1);
        add(r2);
        add(b);
        setSize(300, 300);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (r1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Mele.");
        }
        if(r2.isSelected()){
            JOptionPane.showMessageDialog(this , "Your are Female");
        }
    }
    public static void main(String[] args) {
        new RadioButton2();
    }
}
