/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author Pla
 */
public class MenuItem2 implements ActionListener {

    JFrame f;
    JMenuBar mb;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectALL;
    JTextArea ta;

    MenuItem2() {
        f = new JFrame();
        cut = new JMenuItem("Cut");
        copy = new JMenuItem("Copy");
        paste = new JMenuItem("Paste");
        selectALL = new JMenuItem("selectALL");

        cut.addActionListener(this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectALL.addActionListener(this);

        mb = new JMenuBar();
        file = new JMenu("file");
        edit = new JMenu("edit");
        help = new JMenu("Help");

        edit.add(cut);
        edit.add(copy);
        edit.add(paste);
        edit.add(selectALL);

        mb.add(file);
        mb.add(edit);
        mb.add(help);

        ta = new JTextArea();
        ta.setBounds(5, 5, 360, 320);
        f.add(mb);
        f.add(ta);
        f.setJMenuBar(mb);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==cut){
            ta.cut();
        }if(e.getSource()==paste){
            ta.paste();
        }if(e.getSource()==copy){
            ta.copy();
        }if(e.getSource()==selectALL){
            ta.selectAll();
        }
    }
    public static void main(String[] args) {
        new MenuItem2();
    }
}
