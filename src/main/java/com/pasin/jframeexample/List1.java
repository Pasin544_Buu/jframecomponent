/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;

/**
 *
 * @author Pla
 */
public class List1 extends JFrame{
    List1(){
        
    
    JFrame f =new JFrame();
    DefaultListModel<String> l1 = new  DefaultListModel<>();
    l1.addElement("item1");
    l1.addElement("item2");
    l1.addElement("item3");
    l1.addElement("item4");
    
    JList<String> list = new JList<>(l1);
    list.setBounds(100,100,75,75);
    f.add(list);
    f.setSize(400,400);
    f.setLayout(null);
    f.setVisible(true);
    f.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new List1();
    }
}
