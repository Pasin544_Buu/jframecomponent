/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author Pla
 */
public class DisplayImage extends Canvas{
    public void paint(Graphics g){
        Toolkit t =Toolkit.getDefaultToolkit();
        Image i =t.getImage("D:\\p3.gif");
        g.drawImage(i, 120, 100, this);
        
    }
    public static void main(String[] args) {
        DisplayImage m= new DisplayImage();
        JFrame f =new JFrame();
        f.setSize(400,400);
        f.add(m);
        f.setVisible(true);
    }
}
