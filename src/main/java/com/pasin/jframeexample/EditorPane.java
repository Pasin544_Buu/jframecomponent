/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author Pla
 */
public class EditorPane {
    JFrame myFrame = null;
    
    public static void main(String[] args) {
        (new EditorPane()).test();
    }
    private void test(){
        myFrame = new JFrame("JEditorPane Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setSize(400,200);
        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/plain");
        myPane.setText("Hello My name is Ink Im Toxic Player in dota2"+
                " \nim 2k mmr dota player"+
                " \nBut my Friend need me to help when he need mmr");
        myFrame.setContentPane(myPane);
        myFrame.setVisible(true);
    }
}
