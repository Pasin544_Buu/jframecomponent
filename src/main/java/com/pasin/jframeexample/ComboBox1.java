/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


/**
 *
 * @author Pla
 */
public class ComboBox1 {
    JFrame f;
    ComboBox1(){
        f = new JFrame("ComboBox");
        f.setSize(400,400);
        String Team[]={"EG","OG","Alince","T1","Fanatic"};
        JComboBox cb = new JComboBox(Team);
        cb.setBounds(50,50,90,20);
        f.add(cb);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new ComboBox1();
    }
}
