/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Pla
 */
public class Table1 extends JFrame{
    JFrame f;
    Table1(){
        f = new JFrame();
        String data[][]={{"001","EG","67000"},
                         {"002","OG","65418"},
                         {"003","T1","52315"}};
        String column[]={"ID","Team","Bet"};
        JTable j = new JTable(data,column);
        j.setBounds(30,40,200,300);
        JScrollPane sp=new JScrollPane(j);
        f.add(sp);
        f.setSize(300,400);
        f.setVisible(true);
        f.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
    public static void main(String[] args) {
        new Table1();
    }
}
