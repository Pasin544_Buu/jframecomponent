/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.jframeexample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Pla
 */
public class CheckBoxMenuItem {

    public static void main(String[] args) {
        JFrame frame = new JFrame("JMenu Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);

        JMenuItem menuitem1 = new JMenuItem("Open", KeyEvent.VK_F);
        fileMenu.add(menuitem1);

        JCheckBoxMenuItem caseMenuitem = new JCheckBoxMenuItem("Option_1");
        caseMenuitem.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(caseMenuitem);

        ActionListener aListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                AbstractButton aButton =(AbstractButton) event.getSource();
                boolean selected = aButton.getModel().isSelected();
                String newLabel;
                Icon newIcon;
                if(selected){
                    newLabel = "Value-1";
                }else{
                    newLabel = "Value-2";
                }
                aButton.setText(newLabel);
            }
        };
        caseMenuitem.addActionListener(aListener);
        frame.setJMenuBar(menuBar);
        frame.setSize(350,250);
        frame.setVisible(true);
    }
}
